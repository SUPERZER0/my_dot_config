(require 'global-keys)
(require 'power-line)
(setq default-input-method "ukrainian-computer")

;; backup in one place. flat, no tree structure
(setq backup-directory-alist '(("" . "/tmp/emacs/backup")))
(setq auto-save-file-name-transforms
          `((".*" ,temporary-file-directory t)))

(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

(add-hook 'prog-mode-hook 'company-mode)

(require 'python-config)
(require 'latex-config)

(require 'purpose)

(provide 'global-config)
