(defun set_size_ide ()
  (setq purpose-ide-window '(0 0 100 100))
  (setq max_x (nth 2 purpose-ide-window))
  (setq max_y (nth 3 purpose-ide-window))
  (setq center_percent_width 0.68)
  (setq center_percent_height 0.68)
  (setq right_percent_width (/ (- 1 center_percent_width) 2))
  (setq left_percent_width (/ (- 1 center_percent_width) 2))
  (setq left_max_x (* left_percent_width max_x))
  (setq center_max_x (+ left_max_x (* center_percent_width max_x)))
  (setq purpose-ide-left (list 0 0 left_max_x max_x))
  (setq purpose-ide-center (list left_max_x 0 center_max_x (* center_percent_height max_y)))
  (setq purpose-ide-right (list center_max_x 0 max_x max_y))
  (setq purpose-ide-center-bottom (list left_max_x (* center_percent_height max_y) center_max_x max_y))

  (setq dired_ (append '(:purpose dired :purpose-dedicated t) (list :width left_percent_width :height 0.5)))
  (setq buffers_ (append '(:purpose buffers :purpose-dedicated t) (list :width left_percent_width :height 0.5)))
  
  (setq edit_ (append '(:purpose edit :purpose-dedicated t) (list :width center_percent_width :height center_percent_height)))
  (setq misc_ (append '(:purpose misc :purpose-dedicated t) (list :width (/ center_percent_width 2) :height (- 1 center_percent_height))))
  (setq shell_ (append '(:purpose shell :purpose-dedicated t) (list :width (/ center_percent_width 2) :height (- 1 center_percent_height))))
  (eshell)
  (setq ilist_ (append '(:purpose ilist :purpose-dedicated t) (list :width (- 1 center_percent_width left_percent_width) :height 0.6)))
  (setq todo_ (append '(:purpose todo :purpose-dedicated t) (list :width (- 1 center_percent_width left_percent_width) :height 0.4)))
  
  (setq
   purpose-x-code1--window-layout
   (append
    (append '(nil) (list purpose-ide-window))
    (list (append '(t) (list purpose-ide-left) (list dired_) (list buffers_)))
    (list (append '(t) (list purpose-ide-center) (list edit_) (list (append '(nil) (list purpose-ide-center) (list misc_) (list shell_)))))
    (list (append '(t) (list purpose-ide-right) (list ilist_) (list todo_)))
    )
   )
  )


(defun load-ide ()
  (interactive)
  (purpose-x-code1-setup)
  (add-to-list 'purpose-user-mode-purposes
	       '(org-mode . todo))
  (add-to-list 'purpose-user-mode-purposes
	       '(prog-mode . edit))
  (add-to-list 'purpose-user-mode-purposes
	       '(eshell-mode . shell))
  (add-to-list 'purpose-user-mode-purposes
	       '(shell-mode . shell))
  (add-to-list 'purpose-user-mode-purposes
	       '(help-mode . misc))
  (add-to-list 'purpose-user-mode-purposes
	       '(debugger-mode . misc))
  (add-to-list 'purpose-user-mode-purposes
	       '(python-inferior-mode . misc))
  (add-to-list 'purpose-user-mode-purposes
	       '(inferior-python-mode . misc))
  
  (purpose-compile-user-configuration)
  (purpose-mode 1)
  

;; helper для получения первого буфера с определенным purpose. нужна для прямого переключения на нужный буфер. (ie, на специальный purpose буфер)
(defun get-only-one-buffer-with-purpose (purpose)
  "Get buffers wih purpose"
  (buffer-name (nth 0 (purpose-buffers-with-purpose purpose)))
  )
;; переключитья на dired buffer, к сожалению, на каждую открытую директорию создается буфер, это значит что по прошествии нескольких часов у вас может быть открыто очень много dired буферов. Здесь есть один недостаток. А может можно проще ? Я не нашел как переключить просто на активный буфер с конкретным purpose. Итак, ты сможешь выбрать на какой буфер переключиться
(define-key purpose-mode-map (kbd "M-m f")
  (lambda () (interactive) (purpose-switch-buffer-with-some-purpose 'dired))
  )
;; переключиться на буфер со списком открытых файлов. Здесь все просто супер. Он один. Поэтому нет проблемы.
(define-key purpose-mode-map (kbd "M-m l")
  (lambda () (interactive) (purpose-switch-buffer (get-only-one-buffer-with-purpose 'buffers)))
  )
;; переключиться на один из файлов для редактирования. Та же проблемы что и с dired
(define-key purpose-mode-map (kbd "M-m c")
  (lambda () (interactive) (purpose-switch-buffer-with-some-purpose 'edit))
  )
;; переключиться на буфер с списком определений (функций, классов и тд и тп) в открытом файле для редактирования. Здесь все просто супер. Он один. Поэтому нет проблемы.
(define-key purpose-mode-map (kbd "M-m d")
  (lambda () (interactive)  (purpose-switch-buffer (get-only-one-buffer-with-purpose 'ilist)))
  )
;; переключиться на буфер с списком todo вещей. Об этом расскажу чуть позже. Полезная вещь. И он тоже один
(define-key purpose-mode-map (kbd "M-m t")
  (lambda () (interactive)  (purpose-switch-buffer (get-only-one-buffer-with-purpose 'todo)))
  )

(defconst todo-mode-buffer-name "*CodeTodo*"
  "Name of the buffer that is used to display todo entries.")

;; когда процесс создания todo файла закончился, выполнится эта функция, фактически эта функция откроет файл в read only mode с списком @todo. Можно будет перейти к месту @todo и посмотреть что там за вопрос не решен.
(defun on-org-mode-todo-file-built (process event)
  (find-file (concat (getenv "PWD") "/todo.org"))
  (call-interactively 'read-only-mode)
  )

;; функция отвечающая за генерацию todo файла для org-mode
(defun build-org-mode-file-for-todo ()
  (start-process "Building todo things" "*CodeTodo*" "bash" "-ic" "source ~/.bashrc; collecttodotags")
  (set-process-sentinel (get-process "Building11 todo things") 'on-org-mode-todo-file-built)
  )

;; создание буффера если его еще нет, запуск функций приведенных выше
(defun todo-mode-get-buffer-create ()
    "Return the todo-mode buffer.
If it doesn't exist, create it."
    (or (get-buffer todo-mode-buffer-name)
        (let ((buffer (get-buffer-create todo-mode-buffer-name)))
          (with-current-buffer buffer
            (org-mode)
            (build-org-mode-file-for-todo)
            (pop-to-buffer todo-mode-buffer-name))
          buffer)))

)

(provide 'ide-layout)
