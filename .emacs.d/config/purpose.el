(add-to-list 'load-path "~/.emacs.d/config/layouts")

(require 'ide-layout)
;; (require 'git-layout)

(global-set-key
 (kbd "M-1")
 (lambda ()
   (interactive)
   (let ((buffer  (current-buffer)))
     (call-interactively 'delete-other-windows)
     (set_size_ide)
     (load-ide)
     (purpose-switch-buffer buffer)
     )
   )
 )
;(global-set-key
; (kbd "M-2")
;(lambda ()
;   (interactive)
;   (call-interactively 'delete-other-windows)
;   (git-load)
;   (magit-status)
;   )

(provide 'purpose)
