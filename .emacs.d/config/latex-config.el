(setq LaTeX-item-indent 0)
(add-hook 'LaTeX-mode-hook 'turn-on-auto-fill)
(require 'latex)
(add-hook 'LaTeX-mode-hook (lambda () (local-set-key (kbd "C-c C-c") 'tex-compile)))

(provide 'latex-config)
