(jedi:install-server)
(add-hook 'python-mode-hook 'jedi:setup)
(add-hook 'python-mode-hook 'yas-minor-mode)

;;(defun venv-activate() (call-interactively  #'pyvenv-activate) )
;;(add-hook 'python-mode-hook 'venv-activate)

(setq
 python-shell-interpreter "ipython"
 python-shell-interpreter-args "-i")

(add-hook 'python-mode-hook 'py-isort-before-save)
(add-hook 'python-mode-hook 'python-black-on-save-mode)
(add-hook 'python-mode-hook 'py-autopep8-enable-on-save)
(add-hook 'python-mode-hook 'flycheck-mode)
(add-hook 'python-mode-hook (lambda () (company-mode -1)))



(provide 'python-config)
