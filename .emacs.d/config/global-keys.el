(global-set-key
 (kbd "M-j")
 (lambda ()
   (interactive)
   (call-interactively 'avy-goto-char-2)
   (let '(i 0)
     (while (< i 2)
       (forward-char)
       (setq i (1+ i))
       )
     )
   )
 )

(global-set-key
 (kbd "C-o")
 (lambda ()
   (interactive)
   (call-interactively 'move-beginning-of-line)
   (call-interactively 'open-line)
   (indent-according-to-mode)
   )
 )

(global-set-key
 (kbd "C-k")
 'kill-whole-line
 )

;; (global-set-key (kbd "C-k") 'kill-line)

(global-set-key
 (kbd "M-l")
 'toggle-input-method
 )

(global-set-key
 (kbd "RET")
 (lambda ()
   (interactive)
   (call-interactively 'newline)
   (indent-according-to-mode)
   )
 )

(global-set-key
 (kbd "M-RET")
 (lambda ()
   (interactive)
   (call-interactively 'move-end-of-line)
   (newline)
   (indent-according-to-mode)
   )
)

(global-set-key (kbd "M-n") 'company-complete)
(setq company-show-numbers t)
(provide 'global-keys)
