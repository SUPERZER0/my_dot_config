; list the packages you want
(setq package-list
      `(
	company-auctex
	company-jedi
	magit
	python-black
	pylint
	pyvenv
	py-isort
	yasnippet-snippets
	flycheck
	window-purpose
	markdown-mode
	markdown-preview-mode
	avy
	eshell
	smart-mode-line-powerline-theme
	jedi
	projectile
	)
      )

; list the repositories containing them
(when (>= emacs-major-version 24)
      (require 'package)
      (setq package-enable-at-startup nil)
      (setq package-archives '())
      (package-initialize)
      (add-to-list 'package-archives
                   '("melpa" . "http://melpa.milkbox.net/packages/") t)  
      (add-to-list 'package-archives
                   '("marmalade" . "https://marmalade-repo.org/packages/"))
      (add-to-list 'package-archives
                   '("gnu" . "http://elpa.gnu.org/packages/"))  
      (add-to-list 'package-archives
                   '("org" . "http://orgmode.org/elpa/") t)
      (add-to-list 'package-archives
                   '("tromey" . "http://tromey.com/elpa/") t)
      )
; activate all the packages (in particular autoloads)
(package-initialize)

; fetch the list of packages available 
(unless package-archive-contents
  (package-refresh-contents))

; install the missing packages
(dolist (package package-list)
  (unless (package-installed-p package)
    (package-install package)))

(provide 'package-install)
