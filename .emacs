(add-to-list 'load-path "~/.emacs.d/config/")

(require 'package-install)
(require 'color-theme)
(require 'global-config)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("a27c00821ccfd5a78b01e4f35dc056706dd9ede09a8b90c6955ae6a390eb1c1e" "84d2f9eeb3f82d619ca4bfffe5f157282f4779732f48a5ac1484d94d5ff5b279" default)))
 '(package-selected-packages
   (quote
    (helm company-jedi company-auctex tex-math-preview ## latex-extra auctex-latexmk auctex latex-preview-pane projectile py-isort py-autopep8 jedi jedy smart-mode-line-powerline-theme yasnippet-snippets window-purpose pyvenv python-black pylint markdown-preview-mode magit flycheck avy))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
